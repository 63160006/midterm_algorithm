/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.midalgor;

import java.util.Scanner;

/**
 *
 * @author tud08
 */
public class reverse {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int num = kb.nextInt();//รับ input จำนวนเต็ม ด้วยตัวแปร num
        int[] input = new int[num];//สร้างตัวแปร array input รับตัวเลขจาก num
        for (int i = 0; i < num; i++) {//ใช้ for loop ตัวแรกวนตัวเลข วนตามจำนวน imput num
            input[i] = kb.nextInt();    //นำตัวแปรมารับเลข input จากผู้ใช้
        }
        for (int i = input.length - 1; i >= 0; i--) {//for loop ตัวที่สองเพื่อวนเลขกลับ 
            System.out.print(input[i]+" ");             //ส่ง output ออกมา
        }
    }
}