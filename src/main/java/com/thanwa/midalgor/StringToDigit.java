/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.midalgor;

import java.util.Scanner;

/**
 *
 * @author tud08
 */
public class StringToDigit {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String check = new String();//ประกาศตัวแปร check เป็น String  
        check = kb.next();              //ตัวแปร check รับ input
        int position = 1;                   //
        int sum = 0;                        //ตัวแปร sum เพื่อรับค่าที่แปลงเป็น Integer
        for (int i = check.length() - 1; i >= 0; i--) {//ใช้ for loop เช็คเลขกลับหลังเพื่อเช็คเลขจากหลักหน่วย
            if (check.charAt(i) == '1') {           //เช็คว่า String ที่รับมาเท่ากับ 1 หรือไม่
                sum += 1 * position;                //เก็บค่าไว้ใน sum
            } else if (check.charAt(i) == '2') {   
                sum += 2 * position;                
            } else if (check.charAt(i) == '3') {    
                sum += 3 * position;                
            } else if (check.charAt(i) == '4') {    
                sum += 4 * position;                
            } else if (check.charAt(i) == '5') {    
                sum += 5 * position;                
            } else if (check.charAt(i) == '6') {    
                sum += 6 * position;                
            } else if (check.charAt(i) == '7') {    
                sum += 7 * position;                
            } else if (check.charAt(i) == '8') {    
                sum += 8 * position;                
            } else if (check.charAt(i) == '9') {    
                sum += 9 * position;                
            } else if (check.charAt(0) == '0') {    //ถ้า String ที่รับมาตัวแรกเป็น 0 
                sum *= -1;                              //ทำให้ sum เก็บ Integer เป็นจำนวนติดลบ
            } else if (check.charAt(i) == '-') {    //ถ้า String ที่รับมาเป็น -
                sum *= -1;                              //ทำให้ sum เก็บ Integer เป็นจำนวนติดลบ
            }
            position *= 10;                 //for loop วนครบเมื่อตัวเลขแต่ละหน่วยเพิ่มขึ้น
        }
        System.out.println(sum);        //input คำตอบออกมาที่เป็น Integer
    }
}
